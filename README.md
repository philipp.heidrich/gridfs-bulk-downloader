# NodeJS Mongo GridFS bulk downloader

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

A bulk downloader for MongoDB GridFS files written in NodeJS. This script requried NodeJS, you can download this from the [offical website](https://nodejs.org).<br>
More aboute GridFS can you find here on the [MongoDb GridFS](https://docs.mongodb.com/manual/core/gridfs/) page

## Usage

Install node dependencies

```bash
npm i
```

Run bulk downloader

```bash
./gridfs-bulk-downloader.js --help
```

## Options

Argument | Description | Default | Required
---|---|---|---
--mongo-host <name>             | MongoDB connection url                                   | mongodb://localhost:27017  | false
-d, --mongo-database <name>     | MongoDB connection name                                  |                            | true
--download-folder <name>        | Folder where placed the files from the db                | ./downloaded-files         | false
-m, --max-download-files <name> | Maximal downloaded files. 0 means there are no limits    | 0                          | false
-c, --gridfs-collection <name>  | Which GridFS collection will be downloaded               |                            | true
-f, --filter-documents <name>   | Filter to find the files to downloaded                   | {}                         | false
-h, --help                      | Output usage information                                 |                            | false
