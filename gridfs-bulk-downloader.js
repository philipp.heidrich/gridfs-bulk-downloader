#!/usr/bin/env node

const mongodb = require('mongodb');
const mongoClient = mongodb.MongoClient;
const fs = require('fs');
const fsExtra = require('fs-extra');
const commander = require('./src/commander');

let database;
let fileLoop;
let config = commander();

/**
 * Helper functions
 */
function printMessage(message) {
	console.log('> ' + message);
}

/**
 * Download the file with the given filname
 * via GridFSBucket download stream
 */
function downloadImage(filename, callback, errorCallback) {
	var bucket = new mongodb.GridFSBucket(database, {
		bucketName: config.gridfsCollection
	});

	bucket
		.openDownloadStreamByName(filename)
		.pipe(fs.createWriteStream(config.downloadFolder + '/' + filename))
		.on('error', error => {
			errorCallback(error);
		})
		.on('finish', () => {
			callback();
		});
}

/**
 * Loop Array and wait until the file is download
 */
function loopFileArray(array) {
	downloadImage(
		array[fileLoop].filename,
		() => {
			console.log(array[fileLoop].filename);

			fileLoop++;

			if (fileLoop < array.length) {
				loopFileArray(array);
			} else {
				console.log('');
				printMessage(fileLoop + ' files successful downloaded');
				process.exit(0);
			}
		},
		error => {
			console.log('');
			printMessage(
				'Error occurred by file ' + fileLoop + '/' + config.maxDownloadFiles
			);
			console.error(error);
			process.exit(1);
		}
	);
}

/**
 * Clean up the folder the temporary download folder
 */
function cleanDownloadFolder() {
	console.log('> Clean temp folder');
	fsExtra.emptyDirSync(config.downloadFolder);
}

function getGridFiles() {
	mongoClient.connect(config.mongoHost, (err, client) => {
		if (err) {
			printMessage(
				`Unable to connect to MongoDB database <${config.mongoHost}>`
			);
		} else {
			printMessage('Connect to the Database');

			fileLoop = 0;

			database = client.db(config.mongoDatabase);
			database
				.collection(config.gridfsCollection + '.files')
				.find(config.filterDocuments)
				.limit(config.maxDownloadFiles)
				.toArray((err, docs) => {
					if (err) {
						console.error(err);
						process.exit(1);
					}

					if (!docs || docs.length === 0) {
						console.error("No files found");
						process.exit(1);
					}

					printMessage('Files found');
					loopFileArray(docs);
				});
		}
	});
}

cleanDownloadFolder();
getGridFiles();
