const commander = require('commander');

const cmd = commander
	.usage('[OPTIONS]...')
	.option(
		'--mongo-host <name>',
		'MongoDB connection url',
		'mongodb://localhost:27017'
	)
	.option('-d, --mongo-database <name>', 'MongoDB connection name')
	.option(
		'--download-folder <name>',
		'Folder where placed the files from the db',
		'./downloaded-files'
	)
	.option(
		'-m, --max-download-files <name>',
		'Maximal downloaded files. 0 is no limit',
		0
	)
	.option(
		'-c, --gridfs-collection <name>',
		'Which GridFS collection will be downloaded'
	)
	.option(
		'-f, --filter-documents <name>',
		'Filter to find the files to downloaded',
		{}
	)
	.parse(process.argv);

module.exports = () => {
	if (cmd.mongoDatabase == undefined) {
		console.error('No database was specified');
		process.exit(1);
	}

	if (cmd.filterDocuments == undefined) {
		console.error('Required --filter-documents config is not given');
		process.exit(1);
	}

	if (cmd.filterDocuments == undefined) {
		console.error('Required --filter-documents config is not given');
		process.exit(1);
	}

	cmd.maxDownloadFiles = parseInt(cmd.maxDownloadFiles);
	if (cmd.maxDownloadFiles === 0) {
		cmd.maxDownloadFiles = 99999999999;
	}

	return cmd;
};
